# IoTIoT.in On Boarding

How to read this document ? 

You must have atleast 15 mins of free time, stable Internet, to complete all steps given here.

## Step 1 : Fork the Repository 

![guide](extras/010.PNG)


## Instruction to install git
#### Note : For next Step to work you may need to install git on your machine
For Linux users [follow these steps](https://www.atlassian.com/git/tutorials/install-git#linux)  
For Windows users [follow these steps](https://www.atlassian.com/git/tutorials/install-git#windows)

## Step 2 : For Advanced users

### 1. Enter Terminal 

#### For Linux users
1. Press Ctrl + Alt + T on keyboard

#### For Windows users
1. Press Windos + R on keyboard
2. Type cmd and press Enter

### 2. Clone this repository
```
git clone https://gitlab.com/shunyaos/git-training.git
```
### 3. Run a script using command given below in the terminal

#### For Linux users
```shell
cd git-training
./check_linux.sh
```
#### For Windows users
```shell
cd git-training
check_windows.bat
```
### 4. Commit all your changes (include all  files in your directory)
```
git commit -m "Ran script"
```
### 5. Push the changes to the remote repository
```
git push
```

Microsoft Windows [Version 10.0.17134.1006]
(c) 2018 Microsoft Corporation. All rights reserved.

C:\Users\pc-13>git clone https://gitlab.com/shunyaos/git-training.git
'git' is not recognized as an internal or external command,
operable program or batch file.

C:\Users\pc-13>cd git-trainingcheck_windows.bat
The system cannot find the path specified.

C:\Users\pc-13>git commit -m "Ran script"
'git' is not recognized as an internal or external command,
operable program or batch file.

C:\Users\pc-13>git push
'git' is not recognized as an internal or external command,
operable program or batch file.

C:\Users\pc-13>


# Contribute
Do you have any suggestions to improve this project ? 

Then Please open up an Issue in the project on Gitlab and let us discuss your Ideas/suggestions in detail. Please follow our [contributing guidelines](CONTRIBUTING.md).
